@extends('layouts.layout')
@section('content')
    <?php
    $games = json_decode($category);
    switch ($games->id) {
        case 1 :
            $class = "virtual_reality_inner";
            break;
        case 2 :
            $class = "interactive_games";
            break;
        case 3 :
            $class = "arcade_games";
            break;
        case 4 :
            $class = "room_based";
            break;
        default :
            $class = "virtual_reality_inner";
    }
    ?>
    <div class="{{ $class }}">
        <div class="container">
            <h2 class="game_inner_head">{{ $games->category_name }}</h2>
            @if (count($games->games) == 0)
                <p class="game_inner_info">No games found under this category.</p>
            @else
                <div class="row">
                    <?php
                    $i = 1;
                    ?>
                    @foreach($games->games as $game)
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="game_inner_box">
                                <img src="{{ URL::asset('uploads').'/'.$game->image }}">
                                <h4>{{ $game->title }}</h4>
                            </div>
                        </div>
                        @if ($i == 4)
                            </div><div class="row">
                            <?php
                            $i = 0;
                            ?>
                        @endif
                        <?php
                        $i += 1;
                        ?>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@stop