@extends('layouts.layout')
@section('content')
    <div class="container about_inner">
        <div class="row">
            <h2>About Jixy</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not
                only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s
                with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus
                PageMaker including versions of Lorem Ipsum.</p>


            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 about_inner_chooseus">

                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text
                    ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <h2>Why Choose Us ?</h2>
                <p>For us and all of our clients it's a very easy question to answer.</p>
                <ul>
                    <li>We're clean, professional, &amp; the job is always left clean.</li>
                    <li>We don't leave your home until there is a big smile on your face and you feel strongly about recommending us to your friends and family.</li>
                    <li>We offer the Best pricing..</li>
                    <li>Always on time &amp; on budget.</li>
                </ul>
                <p>These are just a few reasons to choose&nbsp; Nexway Technologies. If you would like more
                    please call us and we will give you a lot more.</p>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <img style="width:100%" src="images/about_inner_bg.jpg">

            </div>
        </div>



    </div>
@stop()