@extends('layouts.layout')
@section('content')
    <div class="container-fluid" style="padding:0">
        <div class="container game_inner">
            <div class="row">
                <h2>Games we provide</h2>
                <p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>
            </div>
        </div>

        {{-- Virtual Reality --}}
        <?php
        $vrGs = json_decode($vrGames);
        ?>
        <div class="virtual_reality_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <img src="{{ URL::asset('uploads').'/'.$vrGs -> image }}">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <h2>{{ $vrGs -> category_name }}</h2>
                            <p>{{ $vrGs -> description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @foreach($vrGs -> get_last_12 as $game)
                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 game_sub_items_main">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 game_sub_items">
                                    <img src="{{ URL::asset('uploads').'/'.$game -> image }}" />
                                    <h3>{{ $game -> title }}</h3>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {{-- /Virtual reality --}}

        {{-- Interactive Games --}}
        <?php
        $igs = json_decode($igGames);
        ?>
        <div class="interactive_games">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <img src="{{ URL::asset('uploads').'/'.$igs -> image }}">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <h2>{{ $igs -> category_name }}</h2>
                            <p>{{ $igs -> description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @foreach($igs -> get_last_12 as $game)
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 game_sub_items_main">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 game_sub_items">
                                        <img src="{{ URL::asset('uploads').'/'.$game -> image }}" />
                                        <h3>{{ $game -> title }}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- /Interactive Games --}}

        {{-- Arcade Games --}}
        <?php
        $ags = json_decode($agGames);
        ?>
        <div class="arcade_games">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <img src="{{ URL::asset('uploads').'/'.$ags -> image }}">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <h2>{{ $ags -> category_name }}</h2>
                            <p>{{ $ags -> description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @foreach($ags -> get_last_12 as $game)
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 game_sub_items_main">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 game_sub_items">
                                        <img src="{{ URL::asset('uploads').'/'.$game -> image }}" />
                                        <h3>{{ $game -> title }}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- /Arcade Games --}}

        {{-- Room Based Games --}}
        <?php
        $rbs = json_decode($rbGames);
        ?>
        <div class="room_based">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <img src="{{ URL::asset('uploads').'/'.$rbs -> image }}">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <h2>{{ $rbs -> category_name }}</h2>
                            <p>{{ $rbs -> description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @foreach($rbs -> get_last_12 as $game)
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 game_sub_items_main">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 game_sub_items">
                                        <img src="{{ URL::asset('uploads').'/'.$game -> image }}" />
                                        <h3>{{ $game -> title }}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- /Room Based Games --}}

        {{-- Edutainment Games --}}
        <?php
        $egs = json_decode($egGames);
        ?>
        <div class="virtual_reality_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <img src="{{ URL::asset('uploads').'/'.$egs -> image }}">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 virtual_reality_left">
                            <h2>{{ $egs -> category_name }}</h2>
                            <p>{{ $egs -> description }}</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @foreach($egs -> get_last_12 as $game)
                                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 game_sub_items_main">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 game_sub_items">
                                        <img src="{{ URL::asset('uploads').'/'.$game -> image }}" />
                                        <h3>{{ $game -> title }}</h3>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- /Edutainment Games --}}


    </div>
@stop()