<?php

namespace App\Http\Controllers;
use App\Banner;
use App\Gallery;
use App\Game;
use App\GameCategory;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bannerCount = Banner::count();
        $galleryCount = Gallery::count();
        $gameCategoryCount = GameCategory::count();
        $gameCount = Game::count();
        return view('admin.dashboard', compact('bannerCount', 'galleryCount', 'gameCategoryCount', 'gameCount'));
    }
}
