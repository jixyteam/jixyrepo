@extends('layouts.layout')
@section('content')
    <div class="container contact_inner">
        <h2>Contact Us</h2>
        <p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style=" height:auto;margin-top:35px; padding:0;">
                    <form id="contact_form" method="post" action="/feedback">
                        {{ csrf_field() }}
                        <input name="name" placeholder="Full Name *" required="" type="text">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong class=" text-danger">{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <input name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Email *" required="" type="email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong class=" text-danger">{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <input name="contact_number" pattern="[0-9]{10}" placeholder="Contact Number *" required="" type="text">
                        @if ($errors->has('contact_number'))
                            <span class="help-block">
                                <strong class=" text-danger">{{ $errors->first('contact_number') }}</strong>
                            </span>
                        @endif
                        <input name="subject" placeholder="Subject" type="text">
                        <textarea name="message" placeholder="Comments" required></textarea>
                        @if ($errors->has('message'))
                            <span class="help-block">
                                <strong class=" text-danger">{{ $errors->first('message') }}</strong>
                            </span>
                        @endif
                        <input class="more1" style="margin-top:0px;cursor:pointer;margin-left:10px" name="" value="Clear" type="reset">
                        <input class="more1" style="margin-top:0px;cursor:pointer" name="submit" value="Submit" type="submit">
                    </form>
                </div>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="font-size:13px;text-align:justify;line-height:24px;margin-top:40px;height:auto;color:#737373">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:auto;padding:0;">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 cnt_head" style="font-size:30px;margin-top:10px;text-align:left;padding:0; font-weight:600; text-transform:uppercase; margin-bottom:20px;color:#DB150F;">Jixy</div>
                    No.145, J.N.Road (100 ft. Road)<br>
                    Near MMDA Metrostation,<br>
                    Arumbakkam,Chennai_600 106 - India.<br>
                    Tel: +91 08041535811<br>
                    Email : info@jixy.in <br>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="height:245px;border:1px solid #A6A6A6;margin-top:20px;padding:0">

                    <iframe style="width:100%; height:243px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21984.64775254578!2d80.21675764704432!3d13.075506876910177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5266a26e195065%3A0x11382e6761fa01e8!2sArumbakkam%2C+Chennai%2C+Tamil+Nadu!5e0!3m2!1sen!2sin!4v1442095461298" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>

                </div>
            </div>

        </div>

    </div>
@stop()