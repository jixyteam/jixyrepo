@extends('layouts.adminlayout')
@section('title', 'Game Categories')
@section('content')
    <section class="content-header">
        <h1>
            Game
            <small>Categories</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Game Categories</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Gallery Categories</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($gameCategories) == 0)
                                    <tr><td colspan="4" class="text-center">No records found</td></tr>
                                @else
                                <?php $i = 1; ?>
                                    @foreach($gameCategories as $gameCategory)
                                        <tr>
                                            <td>{{ $i }}</td>
                                            <td>{{ $gameCategory->category_name }}</td>
                                            <td>{{ $gameCategory->description }}</td>
                                            <td><img src="{{ URL::asset('uploads') }}/{{ $gameCategory->image }}" alt=""></td>
                                        </tr>
                                        <?php $i+=1; ?>
                                    @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop()