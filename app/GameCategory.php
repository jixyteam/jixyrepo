<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameCategory extends Model
{
    public function games () {
        return $this->hasMany(Game::class)->orderBy('created_at', 'desc');
    }
    public function get_last_12 () {
        return $this->hasMany(Game::class)->limit(12)->orderBy('created_at', 'desc');
    }
    public function get_11_vid () {
        return $this->hasMany(Game::class)->limit(11)->orderBy('created_at', 'desc');
    }
    public function addGame (Game $game, $game_category_id) {
        $game -> game_category_id = $game_category_id;
        return $this->games()->save($game);
    }
}
