@extends('layouts.layout')
@section('styles')
    <link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
    <link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
@stop
@section('content')
    <div class="container-fluid gallery_inner">
        <h2>Our Gallery</h2>
        <p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>
        <div id="vlightbox1">
            @foreach($galleries as $gallery)
                <a class="vlightbox1" href="{{ URL::asset('uploads') }}/{{ $gallery->image }}" title="{{ $gallery->caption }}">
                    <img src="{{ URL::asset('uploads') }}/{{ $gallery->image }}" alt="{{ $gallery->caption }}"/>
                </a>
            @endforeach
            {{--<a class="vlightbox1" href="index_files/vlb_images1/1.jpg" title="1"><img src="index_files/vlb_thumbnails1/1.jpg" alt="1"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/2.jpg" title="2"><img src="index_files/vlb_thumbnails1/2.jpg" alt="2"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/3.jpg" title="3"><img src="index_files/vlb_thumbnails1/3.jpg" alt="3"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/4.jpg" title="4"><img src="index_files/vlb_thumbnails1/4.jpg" alt="4"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/5.jpg" title="5"><img src="index_files/vlb_thumbnails1/5.jpg" alt="5"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/6.jpg" title="6"><img src="index_files/vlb_thumbnails1/6.jpg" alt="6"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/7.jpg" title="7"><img src="index_files/vlb_thumbnails1/7.jpg" alt="7"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/8.jpg" title="8"><img src="index_files/vlb_thumbnails1/8.jpg" alt="8"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/1.jpg" title="1"><img src="index_files/vlb_thumbnails1/1.jpg" alt="1"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/2.jpg" title="2"><img src="index_files/vlb_thumbnails1/2.jpg" alt="2"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/3.jpg" title="3"><img src="index_files/vlb_thumbnails1/3.jpg" alt="3"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/4.jpg" title="4"><img src="index_files/vlb_thumbnails1/4.jpg" alt="4"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/5.jpg" title="5"><img src="index_files/vlb_thumbnails1/5.jpg" alt="5"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/6.jpg" title="6"><img src="index_files/vlb_thumbnails1/6.jpg" alt="6"/></a>
            <a class="vlightbox1" href="index_files/vlb_images1/7.jpg" title="7"><img src="index_files/vlb_thumbnails1/7.jpg" alt="7"/></a>--}}

            <a class="vlightbox1" href="index_files/vlb_images1/8.jpg" title="8"><img src="index_files/vlb_thumbnails1/8.jpg" alt="8"/></a>
        </div>
    </div>
@stop()
@section('scripts')
    <script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
    <script src="index_files/vlb_engine/thumbscript1.js" type="text/javascript"></script>
    <script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
@stop()