<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

//use Illuminate\Foundation\Auth\ResetsPasswords;

class ChangePasswordController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function showChangeForm() {
        return view('admin.change-form');
    }
    public function changePassword(Request $request) {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required|min:6|max:255',
            'confirm_password' => 'required|min:6|max:255'
        ]);
        $flag = true;
        if (Auth::user()->email != $request->email) {
            $flag = false;
        }
        if ($request->password != $request->confirm_password) {
            $flag = false;
        }
        if (!$flag) {
            flash_status("danger", "Something went wrong! Please provide valid details.");
            return back();
        } else {
            $curUser = User::find(Auth::user()->id);
            $updateData = array("password" => Hash::make($request->password));
            $curUser->update($updateData);
            //flash_status("success", "Your password has been changed");
            Auth::logout();
            return redirect('/login');
        }
    }
}
