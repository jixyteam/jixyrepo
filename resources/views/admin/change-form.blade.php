@extends('layouts.adminlayout')
@section('title', 'Change Password')
@section('content')
    <section class="content-header">
        <h1>
            Change Password
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Change Password</li>
        </ol>
    </section>
    <section class="content">
        @include('admin/session-flash')
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Change Password</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <form action="/password/change" method="post">
                                    {{ csrf_field() }}
                                    <div class="form-group has-feedback">
                                        <label for="email">Email</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="email" id="email" class="form-control" placeholder="Email" name="email"
                                               value="{{ old('email') }}" required>
                                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="password">New Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" id="password" class="form-control" name="password"
                                               placeholder="New Password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label for="confirm_password">Confirm Password</label> &nbsp;&nbsp;<span class="text-danger">(Required)</span>
                                        <input type="password" id="confirm_password" class="form-control" name="confirm_password"
                                               placeholder="Confirm Password Password" required>
                                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                        @if ($errors->has('confirm_password'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('confirm_password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop