<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jixy</title>
    <!--<link rel="stylesheet" type="text/css" href="css/amazingslider-1.css">-->
    <link rel="stylesheet" type="text/css" href="sliderengine/amazingslider-2.css">
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
    <link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
    <style>
        .header_bottom{z-index: 999999;}
        .amazingslider-bottom-shadow-2{ display:none !important}
        .amazingslider-space-2{ width:100% !important; height:auto !important}
        .amazingslider-swipe-box-2{width:100% !important; height:500px !important}
        #amazingslider-2{width:100% !important;}
        #amazingslider-wrapper-2{width:100% !important;padding-right:135px !important;max-width: 80% !important;}
        .amazingslider-img-2 img{ height:100% !important}
        @media screen and (max-width:1255px){
            .amazingslider-swipe-box-2{width:100% !important; height:400px !important}}
        @media screen and (max-width:991px){
            .amazingslider-swipe-box-2{width:100% !important; height:350px !important}}
        @media screen and (max-width:860px){
            .amazingslider-swipe-box-2{width:100% !important; height:300px !important}}
    </style>
    <link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
<div id="amazingslider-wrapper-1">
    <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
        <ul class="amazingslider-slides" style="display:none;">
            @foreach($banners as $banner)
                <li><img src="{{ URL::asset('uploads').'/'.$banner->image }}" alt=""></li>
            @endforeach
        </ul>
    </div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 header_top">

        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
            <p><span>EMAIL SUPPORT</span><br> info@jixigames.com</p>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 support_small">
            <p><span>CALL SUPPORT</span><br>+919065895634</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 support_small">
            <p><span>WORKING HOURS</span><br> Mon - Sat 8:30am - 7:30pm</p>
        </div>

    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 top_logo"><img src="images/logo.png"></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 header_bottom">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li {{ (Request::is('/')) ? 'class=active' : '' }}><a href="/">Home</a></li>
                <li {{ (Request::is('about')) ? 'class=active' : '' }}><a href="/about">About Us</a></li>
                <li {{ (Request::is('games')) ? 'class=active' : '' }}><a href="/games">Games</a></li>
                <li {{ (Request::is('videos')) ? 'class=active' : '' }}><a href="/videos">Videos</a></li>
                <li {{ (Request::is('gallery')) ? 'class=active' : '' }}><a href="/gallery">Gallery</a></li>
                <li {{ (Request::is('contact')) ? 'class=active' : '' }}><a href="/contact">Contact Us</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container second_section" style="display:inline-block">
    <div id="amazingslider-wrapper-2" style="display:block;position:relative;max-width:850px;padding-left:0px; padding-right:299px;margin:0px auto 0px;">
        <div id="amazingslider-2" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                @foreach($videos as $video)
                    <li>
                        <img src="{{ URL::asset('uploads').'/'.$video->image }}" />
                        <video preload="none" src="{{ $video->video_id }}" ></video>
                    </li>
                @endforeach
            </ul>
            <ul class="amazingslider-thumbnails" style="display:none;">
                @foreach($videos as $video)
                    <li><img src="{{ URL::asset('uploads').'/'.$video->image }}" /></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
<div class="virtual_reality">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                    <img src="{{ URL::asset('uploads').'/'.$gameCategories[0]->image }}">
                    <h2>{{ $gameCategories[0]->category_name }}</h2>
                    <p>{{ $gameCategories[0]->description }}</p>
                    <a href="/games/{{ $gameCategories[0]->id }}">View More</a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_right">
                    <img src="images/virtual-reality1.jpg">
                    <img src="images/virtual-reality2.jpg">
                    <img src="images/virtual-reality3.jpg">
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        @foreach($gameCategories as $gameCategory)
            @if ($gameCategory->id != 1)
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 service_itms_frnt">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><h3>{{ $gameCategory->category_name }}</h3></div>
                        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12"><img src="{{ URL::asset('uploads').'/'.$gameCategory->image }}"></div>
                        <div class="col-lg-6 col-md-12 col-sm-6 col-xs-12">
                            <p>{{ limit_string_by_words($gameCategory->description, 25) }}</p>
                            <a href="/games/{{ $gameCategory->id }}">View More</a>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    </div>
</div>
<div class="container-fluid gallery_main">
    <h2>Our Gallery</h2>
    <div id="vlightbox1">
        @foreach($galleries as $gallery)
            <a class="vlightbox1" href="{{ URL::asset('uploads').'/'.$gallery->image }}" title="{{ $gallery->caption }}"><img src="{{ URL::asset('uploads').'/'.$gallery->image }}" alt="{{ $gallery->caption }}"/></a>
        @endforeach
        {{--<a class="vlightbox1" href="index_files/vlb_images1/1.jpg" title="1"><img src="index_files/vlb_thumbnails1/1.jpg" alt="1"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/2.jpg" title="2"><img src="index_files/vlb_thumbnails1/2.jpg" alt="2"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/3.jpg" title="3"><img src="index_files/vlb_thumbnails1/3.jpg" alt="3"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/4.jpg" title="4"><img src="index_files/vlb_thumbnails1/4.jpg" alt="4"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/5.jpg" title="5"><img src="index_files/vlb_thumbnails1/5.jpg" alt="5"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/6.jpg" title="6"><img src="index_files/vlb_thumbnails1/6.jpg" alt="6"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/7.jpg" title="7"><img src="index_files/vlb_thumbnails1/7.jpg" alt="7"/></a>
        <a class="vlightbox1" href="index_files/vlb_images1/8.jpg" title="8"><img src="index_files/vlb_thumbnails1/8.jpg" alt="8"/></a>--}}
    </div>
</div>

<footer>
    <div class="container">
        <div class="row foot-row">

            <!--1st Col close here-->
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="tab_2">
                    <div class="quick">
                        <p class="con_head">important links</p>
                    </div>
                    <div class="foot_nav">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/gallery">Our Gallery</a></li>
                            <li><a href="/games">Games</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--2nd Col starts here-->
            <div class="col-lg-2 col-md-2 col-sm-4">
                <div class="tab_2">

                    <div class="quick">
                        <p class="con_head" style="padding-top:25px;"></p>
                    </div>
                    <div class="foot_nav">
                        <ul>
                            <li><a href="/videos">Videos</a></li>
                            <li><a href="/contact">contact Us</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-bord">
                <div class="tab_2">
                    <div class="addrs">
                        <p class="con_head">Quick CONTACT</p>
                        <p><img src="images/locatin.png">Lorem ipsum dolor sit amet, Calicut
                            <br> <span>MP Road, Calicut- 673001</span>
                            <br> <span>Kerala, India</span>
                            <br><img src="images/call.png">+91 9847 123456 / 996 123456
                    </div>
                </div>
            </div>
            <!--1st Col starts here-->
            <div class="col-lg-3 col-md-3 col-sm-3 col-bord">
                <iframe style="width:100%; height:150px; margin-top:20px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497699.99740305037!2d77.35073679427201!3d12.953847717752192!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C+Karnataka!5e0!3m2!1sen!2sin!4v1476270637230" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>

        </div>
    </div>
    <!--Copyrigth bar starts here-->

    <div class="container-fluid footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="foot_bar">
                        <div class="copy"> © 2016 Jixy , All rights reserved, Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png"></a> </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--Copyrigth bar close here-->

</footer>

{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>--}}
<script src="js/jquery-2.2.4.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}
<script src="js/bootstrap.min.js"></script>
<!-- <script src="js/jquery.js"></script>-->
<!--<script src="js/amazingslider.js"></script>-->
<script src="sliderengine/jquery.js"></script>
<script src="js/initslider-1.js"></script>




<script src="sliderengine/amazingslider.js"></script>
<link rel="stylesheet" type="text/css" href="sliderengine/amazingslider-2.css">
<script src="sliderengine/initslider-2.js"></script>


<!-- <script src="index_files/vlb_engine/jquery.min.js" type="text/javascript"></script>-->
<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
<script src="index_files/vlb_engine/thumbscript1.js" type="text/javascript"></script>
<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>



</body>
</html>