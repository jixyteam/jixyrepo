@extends('layouts.adminlayout')
@section('title', 'Banners')
@section('content')
    <section class="content-header">
        <h1>
            Banners
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Banners</li>
        </ol>
    </section>
    <section class="content">
        @include('admin/session-flash')
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">List of Banner Images</h3>
                        <span class="pull-right"><a href="/admin/banner/add" class="btn btn-primary btn-flat">Add New</a></span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Sl No</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($banners) == 0)
                                <tr><td colspan="4" class="text-center">No records found</td></tr>
                            @else
                                <?php $i = 1; ?>
                                @foreach($banners as $banner)
                                    <tr>
                                        <td>{{ $i }}</td>
                                        <td><img src="{{ URL::asset('uploads') }}/{{ $banner->image }}" alt=""></td>
                                        <td>
                                            <a href="#" class="btn btn-danger btn-flat" data-action="trigger_form">
                                                Delete
                                                <form method="POST" action="/admin/banner/{{ $banner->id }}" class="hidden_form">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                </form>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php $i+=1; ?>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop()