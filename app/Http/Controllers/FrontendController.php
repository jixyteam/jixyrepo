<?php

namespace App\Http\Controllers;
use App\Gallery;
use App\GameCategory;
use App\Game;
use App\Banner;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    // Home
    public function index () {
        $banners = Banner::orderBy('created_at', 'desc')->get();
        $galleries = Gallery::orderBy('created_at', 'desc')->limit(8)->get();
        $gameCategories = GameCategory::all();
        $videos = Game::orderBy('created_at', 'desc')->limit(5)->get();
        return view('frontend.index', compact('banners', 'galleries', 'gameCategories', 'videos'));
    }
    // About Us
    public function about () {
        return view('frontend.about');
    }
    // Games
    public function games () {
        $vrGames = GameCategory::with('get_last_12') -> find(1);
        $igGames = GameCategory::with('get_last_12') -> find(2);
        $agGames = GameCategory::with('get_last_12') -> find(3);
        $rbGames = GameCategory::with('get_last_12') -> find(4);
        $egGames = GameCategory::with('get_last_12') -> find(5);
        //return $vrGames;
        return view('frontend.games', compact('vrGames', 'igGames', 'agGames', 'rbGames', 'egGames'));
    }
    // Videos
    public function videos () {
        $vrVids = GameCategory::with('get_11_vid')->find(1);
        $igVids = GameCategory::with('get_11_vid')->find(2);
        $agVids = GameCategory::with('get_11_vid')->find(3);
        $rbVids = GameCategory::with('get_11_vid')->find(4);
        $egVids = GameCategory::with('get_11_vid')->find(5);
        return view('frontend.videos', compact('vrVids', 'igVids', 'agVids', 'rbVids', 'egVids'));
    }
    // Gallery
    public function gallery () {
        $galleries = Gallery::all();
        return view('frontend.gallery', compact('galleries'));
    }
    // Contact Us
    public function contact () {
        return view('frontend.contact');
    }

    // Games inner page
    public function gameInner (GameCategory $category) {
        $category->load('games');
        return view('frontend.game-inner', compact('category'));
    }

    // Videos Inner
    public function videoInner(GameCategory $category) {
        $category->load('games');
        return view('frontend.video-inner', compact('category'));
    }

    // Feedback mail
    public function feedback (Request $request) {
        $this -> validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'contact_number' => 'required',
            'message' => 'required'
        ]);
        $feedbackData = $request->except('_token');
        Mail::to("casc@gmail.com")->send(new FeedbackMail($feedbackData));
        return back();
    }
}
