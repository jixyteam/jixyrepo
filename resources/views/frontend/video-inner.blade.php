@extends('layouts.layout')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css') }}/lightgallery.css">
@stop()
@section('content')
    <?php
    $videos = json_decode($category);
    switch ($videos->id) {
        case 1 :
            $class = "virtual_reality_inner";
            break;
        case 2 :
            $class = "interactive_games";
            break;
        case 3 :
            $class = "arcade_games";
            break;
        case 4 :
            $class = "room_based";
            break;
        default :
            $class = "virtual_reality_inner";
    }
    ?>
    <div class="{{ $class }} video_inner_wrap">
        <div class="container">
            <h2 class="game_inner_head">{{ $videos->category_name }}</h2>
            @if (count($videos->games) == 0)
                <p class="game_inner_info">No games found under this category.</p>
            @else
                <div class="row">
                    <?php
                    $i = 1;
                    ?>
                    @foreach($videos->games as $video)
                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <div class="video_img_wrap">
                                    <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                    <span><img src="{{ URL::asset('images') }}/play.png" alt="Watch"></span>
                                </div>
                                <h3>{{ $video->title }}</h3>
                            </a>
                        </div>
                        @if ($i == 4)
                </div><div class="row">
                    <?php
                    $i = 0;
                    ?>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@stop
@section('scripts')
    <script src="{{ URL::asset('js') }}/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-fullscreen.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-thumbnail.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-video.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.video_inner_wrap').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.video_inner_wrap a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        });
    </script>
@stop()