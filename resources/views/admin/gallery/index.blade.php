@extends('layouts.adminlayout')
@section('title', 'Gallery')
@section('content')
<section class="content-header">
    <h1>
        Gallery Images
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gallery</li>
    </ol>
</section>
<section class="content">
    @include('admin/session-flash')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">List of Gallery Images</h3>
                    <span class="pull-right"><a href="/admin/gallery/add" class="btn btn-primary btn-flat">Add New</a></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Image</th>
                            <th>Caption</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($galleries) == 0)
                        <tr><td colspan="4" class="text-center">No records found</td></tr>
                        @else
                        <?php $i = 1; ?>
                        @foreach($galleries as $gallery)
                        <tr>
                            <td>{{ $i }}</td>
                            <td><img src="{{ URL::asset('uploads') }}/{{ $gallery->image }}" alt=""></td>
                            <td>{{ $gallery->caption }}</td>
                            <td>
                                <a href="/admin/gallery/{{ $gallery->id }}/edit" class="btn btn-facebook btn-flat">Edit</a>
                                <a href="#" class="btn btn-danger btn-flat" data-action="trigger_form">
                                    Delete
                                    <form method="POST" action="/admin/gallery/{{ $gallery->id }}/delete" class="hidden_form">
                                        {{ method_field('DELETE') }}
                                        {{ csrf_field() }}
                                    </form>
                                </a>
                            </td>
                        </tr>
                        <?php $i+=1; ?>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop()