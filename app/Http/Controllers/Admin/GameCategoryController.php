<?php

namespace App\Http\Controllers\Admin;
use App\GameCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameCategoryController extends Controller
{

    // Authentication
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Index
    public function index () {
        $gameCategories = GameCategory::all();
        return view('admin.game-category.index', compact('gameCategories'));
    }
}
