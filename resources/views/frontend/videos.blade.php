@extends('layouts.layout')
@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css') }}/lightgallery.css">
@stop()
@section('content')
        <div class="container game_inner">
            <div class="row">
                <h2>Games we provide(Videos)</h2>
                <p>Lorem ipsum dolor sit amet, consectettur adipiscing elit</p>
            </div>
        </div>

        {{-- Virtual Reality game videos --}}
        <?php
        $vrVideos = json_decode($vrVids);
        ?>
        <div class="virtual_reality_inner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                        <h2 style="text-align:center">{{ $vrVideos->category_name }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 video_left">
                        <?php
                        $i = 1;
                        ?>
                        @foreach($vrVideos->get_11_vid as $video)
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <div class="video_img_wrap">
                                    <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                    <span><img src="{{ URL::asset('images') }}/play.png" alt="Watch"></span>
                                </div>
                                <h3>{{ $video->title }}</h3>
                            </a>
                            @if ($i == 1)
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 2)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 3)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 4)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 5)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 6)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 7)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 8)
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 9)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 10)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 11)
                    </div>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
                <div class="vid_more_lnk">
                    <a href="/videos/{{ $vrVideos->id }}">Watch More</a>
                </div>
            </div>
        </div>
        {{-- /Virtual Reality Game Videos --}}

        {{-- Iteractive Game Videos --}}
        <?php
        $igVideos = json_decode($igVids);
        ?>
        <div class="interactive_games">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                        <h2 style="text-align:center">{{ $igVideos->category_name }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 video_left">
                        <?php
                        $i = 1;
                        ?>
                        @foreach($igVideos->get_11_vid as $video)
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                <h3>{{ $video->title }}</h3>
                            </a>
                            @if ($i == 1)
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 2)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 3)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 4)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 5)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 6)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 7)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 8)
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 9)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 10)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 11)
                    </div>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
                <div class="vid_more_lnk">
                    <a href="/videos/{{ $igVideos->id }}">Watch More</a>
                </div>
            </div>
        </div>
        {{-- /Interactive Game Videos --}}

        {{-- Arcade Game Videos --}}
        <?php
        $agVideos = json_decode($agVids);
        ?>
        <div class="arcade_games">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                        <h2 style="text-align:center">{{ $agVideos->category_name }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 video_left">
                        <?php
                        $i = 1;
                        ?>
                        @foreach($agVideos->get_11_vid as $video)
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                <h3>{{ $video->title }}</h3>
                            </a>
                            @if ($i == 1)
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 2)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 3)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 4)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 5)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 6)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 7)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 8)
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 9)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 10)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 11)
                    </div>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
                <div class="vid_more_lnk">
                    <a href="/videos/{{ $agVideos->id }}">Watch More</a>
                </div>
            </div>
        </div>
        {{-- /Arcade Game Videos --}}

        {{-- Room Based Game Videos --}}
        <?php
        $rbVideos = json_decode($rbVids);
        ?>
        <div class="room_based">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                        <h2 style="text-align:center">{{ $rbVideos->category_name }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 video_left">
                        <?php
                        $i = 1;
                        ?>
                        @foreach($rbVideos->get_11_vid as $video)
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                <h3>{{ $video->title }}</h3>
                            </a>
                            @if ($i == 1)
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 2)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 3)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 4)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 5)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 6)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 7)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 8)
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 9)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 10)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 11)
                    </div>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
                <div class="vid_more_lnk">
                    <a href="/videos/{{ $rbVideos->id }}">Watch More</a>
                </div>
            </div>
        </div>
        {{-- /Room Based Game Videos --}}

        {{-- Edutainment Game Videos --}}
        <?php
        $egVideos = json_decode($egVids);
        ?>
        <div class="edutainment_games">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 virtual_reality_left">
                        <h2 style="text-align:center">{{ $egVideos->category_name }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 video_left">
                        <?php
                        $i = 1;
                        ?>
                        @foreach($egVideos->get_11_vid as $video)
                            <a class="video_bg" href="https://youtube.com/watch?v={{ $video->video_id }}">
                                <img src="{{ URL::asset('uploads').'/'.$video->image }}" alt="">
                                <h3>{{ $video->title }}</h3>
                            </a>
                            @if ($i == 1)
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 2)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 3)
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 4)
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6">
                                @elseif($i == 5)
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 6)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 7)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 8)
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 9)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 10)
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        @elseif($i == 11)
                    </div>
                    @endif
                    <?php
                    $i += 1;
                    ?>
                    @endforeach
                </div>
                <div class="vid_more_lnk">
                    <a href="/videos/{{ $egVideos->id }}">Watch More</a>
                </div>
            </div>
        </div>
        {{-- /Edutainment Game Videos --}}
@stop()
@section('scripts')
    <script src="{{ URL::asset('js') }}/lightgallery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-fullscreen.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-thumbnail.min.js"></script>
    <script src="{{ URL::asset('js') }}/lg-video.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $('.virtual_reality_inner').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.virtual_reality_inner a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
            $('.interactive_games').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.interactive_games a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
            $('.arcade_games').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.arcade_games a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
            $('.room_based').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.room_based a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
            $('.edutainment_games').lightGallery({
                download: false,
                thumbnail: false,
                fullScreen: false,
                selector: '.edutainment_games a.video_bg',
                youtubePlayerParams: {
                    modestbranding: 1,
                    showinfo: 0,
                    rel: 0,
                    controls: 1
                }
            });
        });
    </script>
@stop()