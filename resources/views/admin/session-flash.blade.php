@if (session()->has('message'))
    <div class="status status-{{ session('status') }}" data-role="auto-hide">
        {{ session('message') }}
    </div>
@endif