@extends('layouts.adminlayout')
@section('title', 'Games')
@section('content')
<section class="content-header">
    <h1>
        Games
    </h1>
    <ol class="breadcrumb">
        <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Games</li>
    </ol>
</section>
<section class="content">
    @include('admin/session-flash')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">List of Games</h3>
                    <span class="pull-right"><a href="/admin/games/add" class="btn btn-primary btn-flat">Add New</a></span>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Sl No</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Image</th>
                            <th>Video</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($games) == 0)
                            <tr><td colspan="4" class="text-center">No records found</td></tr>
                        @else
                            <?php $i = 1; ?>
                            @foreach($games as $game)
                                <?php $gameItem = json_decode($game) ?>
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $gameItem->title }}</td>
                                    <td>{{ $gameItem->game_category->category_name }}</td>
                                    <td><img src="{{ URL::asset('uploads') }}/{{ $gameItem->image }}" alt=""></td>
                                    <td><a href="https://www.youtube.com/watch?v={{ $gameItem->video_id }}" target="_blank" class="btn btn-primary btn-flat">Watch</a></td>
                                    <td>
                                        <a href="/admin/games/{{ $game->id }}/edit" class="btn btn-facebook btn-flat">Edit</a>
                                        <a href="#" class="btn btn-danger btn-flat" data-action="trigger_form">
                                            Delete
                                            <form method="POST" action="/admin/games/{{ $gameItem->id }}/delete" class="hidden_form">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                            </form>
                                        </a>
                                    </td>
                                </tr>
                                <?php $i+=1; ?>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>
@stop()