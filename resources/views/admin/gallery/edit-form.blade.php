@extends('layouts.adminlayout')
@section('title', 'Gallery')
@section('content')
    <section class="content-header">
        <h1>
            Edit Gallery Image
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Gallery</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Gallery Image</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <form action="/admin/gallery/{{ $gallery->id }}" method="POST" enctype="multipart/form-data">
                                    {{ method_field('PATCH') }}
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="exampleInputFile">Image</label>
                                        <input id="exampleInputFile" type="file" name="image" data-action="show_thumbnail">
                                        <img class="image_upload_preview" src="{{ URL::asset('uploads') }}/{{ $gallery->image }}" alt="">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="caption">Caption</label> <span class="text-danger">(Required)</span>
                                        <input type="text" class="form-control" placeholder="Caption" name="caption" id="caption" value="{{ $gallery->caption }}" required>
                                        @if ($errors->has('caption'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('caption') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop()