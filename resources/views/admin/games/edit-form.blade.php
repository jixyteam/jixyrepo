@extends('layouts.adminlayout')
@section('title', 'Games')
@section('content')
    <section class="content-header">
        <h1>
            Edit Game
        </h1>
        <ol class="breadcrumb">
            <li><a href="/admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Edit Game</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Edit Game</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <form action="/admin/games/{{ $game -> id }}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PATCH') }}
                                    <div class="form-group">
                                        <label for="title">Title</label> <span class="text-danger">(Required)</span>
                                        <input type="text" class="form-control" placeholder="Title" name="title" id="title" required value="{{ $game -> title }}">
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="categories">Category</label> <span class="text-danger">(Required)</span>
                                        <select class="form-control" name="game_category_id" id="categories" required>
                                            <option value="">Select Category</option>
                                            @foreach($categories as $category)
                                                <option value="{{ $category -> id }}" {{ ($game -> game_category_id == $category -> id) ? 'selected' : '' }}>{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('game_category_id'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('game_category_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Image</label> <span>768x614</span>
                                        <input id="exampleInputFile" type="file" name="image" data-action="show_thumbnail">
                                        <img class="image_upload_preview" src="{{ URL::asset('uploads') }}/{{ $game -> image }}" alt="">
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <label for="video_id">Video ID</label>
                                        <input type="text" class="form-control" placeholder="Youtube video ID" name="video_id" id="video_id" value="{{ $game -> video_id }}">
                                        @if ($errors->has('video_id'))
                                            <span class="help-block">
                                                <strong class=" text-danger">{{ $errors->first('video_id') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-flat">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@stop()