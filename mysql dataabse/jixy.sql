-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: May 30, 2017 at 09:52 PM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `jixy`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `created_at`, `updated_at`) VALUES
(2, 'banner/1477542905.jpg', '2016-10-27 11:35:05', '2016-10-27 11:35:05'),
(3, 'banner/1477542919.jpg', '2016-10-27 11:35:19', '2016-10-27 11:35:19'),
(4, 'banner/1477542927.jpg', '2016-10-27 11:35:27', '2016-10-27 11:35:27');

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `caption`, `image`, `created_at`, `updated_at`) VALUES
(2, 'Game 1', 'gallery/1477547738.jpg', '2016-10-27 12:55:38', '2016-10-27 12:55:38'),
(3, 'Gkjn', 'gallery/1477547755.jpg', '2016-10-27 12:55:55', '2016-10-27 12:55:55'),
(4, 'nmndh', 'gallery/1477547764.jpg', '2016-10-27 12:56:04', '2016-10-27 12:56:04'),
(5, 'mhgmgjm', 'gallery/1477547779.jpg', '2016-10-27 12:56:19', '2016-10-27 12:56:19'),
(6, 'nnyddfg', 'gallery/1477547791.jpg', '2016-10-27 12:56:31', '2016-10-27 12:56:31'),
(7, 'vfdbsdgb', 'gallery/1477547805.jpg', '2016-10-27 12:56:45', '2016-10-27 12:56:45'),
(8, 'bfaebfdb', 'gallery/1477548649.jpg', '2016-10-27 13:10:49', '2016-10-27 13:10:49'),
(9, 'bdbndgn', 'gallery/1477548658.png', '2016-10-27 13:10:58', '2016-10-27 13:10:58'),
(10, 'dfhbrth', 'gallery/1477548669.jpg', '2016-10-27 13:11:09', '2016-10-27 13:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `game_category_id` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `video_id` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `games_game_category_id_index` (`game_category_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `game_category_id`, `title`, `image`, `video_id`, `created_at`, `updated_at`) VALUES
(36, 1, 'sjrtjryj', 'games/1477566552.jpg', 'jrtjy6j', '2016-10-27 18:09:12', '2016-10-27 18:09:12'),
(35, 1, 'jtrjj', 'games/1477566540.jpg', 'jrtjyrj', '2016-10-27 18:09:00', '2016-10-27 18:09:00'),
(34, 1, 'hthh', 'games/1477566526.jpg', 'hetjhtrj', '2016-10-27 18:08:46', '2016-10-27 18:08:46'),
(33, 1, 'hthrth', 'games/1477566507.jpg', 'htrhrth', '2016-10-27 18:08:27', '2016-10-27 18:08:27'),
(32, 1, 'kyukm', 'games/1477566494.jpg', 'ngfsngn', '2016-10-27 18:08:14', '2016-10-27 18:08:14'),
(31, 1, 'gfmnfgm', 'games/1477566478.jpg', 'mfhmhg', '2016-10-27 18:07:58', '2016-10-27 18:07:58'),
(43, 1, 'gerger', 'games/1477567162.jpg', 'regrsg', '2016-10-27 18:19:22', '2016-10-27 18:19:22'),
(37, 1, 'ascasc', 'games/1477567067.jpg', 'cqacs', '2016-10-27 18:17:47', '2016-10-27 18:17:47'),
(38, 1, 'bebsdf', 'games/1477567086.jpg', 'vavdfv', '2016-10-27 18:18:06', '2016-10-27 18:18:06'),
(39, 1, 'vehbterh', 'games/1477567098.jpg', 'erherh', '2016-10-27 18:18:18', '2016-10-27 18:18:18'),
(40, 1, 'gerheqrh', 'games/1477567115.jpg', 'gerger', '2016-10-27 18:18:35', '2016-10-27 18:18:35'),
(41, 1, 'vfebeb', 'games/1477567128.jpg', 'gregerg', '2016-10-27 18:18:48', '2016-10-27 18:18:48'),
(42, 1, 'brtwht4', 'games/1477567145.jpg', 'greqgher', '2016-10-27 18:19:05', '2016-10-27 18:19:05'),
(44, 2, 'ascas', 'games/1477567687.jpg', 'vrfbvsv', '2016-10-27 18:28:07', '2016-10-27 18:28:07'),
(45, 2, 'cadvsd', 'games/1477567702.jpg', 'fewgw', '2016-10-27 18:28:22', '2016-10-27 18:28:22'),
(46, 2, 'erbheb', 'games/1477567716.jpg', 'fwegw', '2016-10-27 18:28:36', '2016-10-27 18:28:36'),
(47, 2, 'fewgwrg', 'games/1477567731.jpg', 'wegegwe', '2016-10-27 18:28:51', '2016-10-27 18:28:51'),
(48, 2, 'fenern', 'games/1477567790.jpg', 'ntaner', '2016-10-27 18:29:50', '2016-10-27 18:29:50'),
(49, 2, 'brtjw', 'games/1477567803.jpg', 'hweth', '2016-10-27 18:30:03', '2016-10-27 18:30:03'),
(50, 2, 'vwrgerg', 'games/1477567833.jpg', 'rherh', '2016-10-27 18:30:33', '2016-10-27 18:30:33'),
(51, 2, 'gwegqe', 'games/1477567849.jpg', 'gQWEG', '2016-10-27 18:30:49', '2016-10-27 18:30:49'),
(52, 2, 'betherh', 'games/1477567863.jpg', 'gregwer', '2016-10-27 18:31:03', '2016-10-27 18:31:03'),
(53, 2, 'rehehr', 'games/1477567889.jpg', 'fweghrg', '2016-10-27 18:31:29', '2016-10-27 18:31:29'),
(54, 2, 'gwgwrg', 'games/1477567905.jpg', 'rwgwerg', '2016-10-27 18:31:45', '2016-10-27 18:31:45'),
(55, 2, 'gwrgwerg', 'games/1477567923.jpg', 'gwrgwaeg', '2016-10-27 18:32:03', '2016-10-27 18:32:03'),
(56, 2, 'gwegwaeg', 'games/1477567937.jpg', 'fqewg', '2016-10-27 18:32:17', '2016-10-27 18:32:17'),
(57, 3, 'vwdvbsdb', 'games/1477568021.jpg', 'brbsd', '2016-10-27 18:33:41', '2016-10-27 18:33:41'),
(58, 3, 'vdsvsdv', 'games/1477568039.jpg', 'vwqerh', '2016-10-27 18:33:59', '2016-10-27 18:33:59'),
(59, 3, 'bebsdrb', 'games/1477568054.jpg', 'gwergweg', '2016-10-27 18:34:14', '2016-10-27 18:34:14'),
(60, 3, 'gweweg', 'games/1477568067.jpg', 'gwrgw', '2016-10-27 18:34:27', '2016-10-27 18:34:27'),
(61, 3, 'gwegr', 'games/1477568081.jpg', 'gwrghr', '2016-10-27 18:34:41', '2016-10-27 18:34:41'),
(62, 3, 'fwefewf', 'games/1477568095.jpg', 'fefew', '2016-10-27 18:34:55', '2016-10-27 18:34:55'),
(63, 3, 'fefgwrg', 'games/1477568109.jpg', 'eryewrt', '2016-10-27 18:35:09', '2016-10-27 18:35:09'),
(64, 3, 'jktu', 'games/1477568122.jpg', 'hthse', '2016-10-27 18:35:22', '2016-10-27 18:35:22'),
(65, 3, 'vwgrgwerg', 'games/1477568135.jpg', 'feqwfawe', '2016-10-27 18:35:35', '2016-10-27 18:35:35'),
(66, 3, 'adsvdsbv', 'games/1477568736.jpg', 'bsdbs', '2016-10-27 18:45:36', '2016-10-27 18:45:36'),
(67, 3, 'vsdgbearg', 'games/1477568766.jpg', 'gwrhgrweg', '2016-10-27 18:46:06', '2016-10-27 18:46:06'),
(68, 3, 'trjtyjkuyk', 'games/1477568781.jpg', 'jrtkj', '2016-10-27 18:46:21', '2016-10-27 18:46:21'),
(69, 3, 'ukrtkrtuk', 'games/1477568796.jpg', 'ketrk', '2016-10-27 18:46:36', '2016-10-27 18:46:36'),
(70, 4, 'dsvasfvsd', 'games/1477569445.jpg', 'wrgvbserfbv', '2016-10-27 18:57:25', '2016-10-27 18:57:25'),
(71, 4, 'grtjhtyj', 'games/1477569460.jpg', 'jryjry', '2016-10-27 18:57:40', '2016-10-27 18:57:40'),
(72, 4, 'ytjmtfum', 'games/1477569477.jpg', 'utkrum', '2016-10-27 18:57:57', '2016-10-27 18:57:57'),
(73, 4, 'geshwtrj', 'games/1477569501.jpg', 'jwreje', '2016-10-27 18:58:21', '2016-10-27 18:58:21'),
(74, 4, 'btyjntyjn', 'games/1477569579.jpg', 'gethset4wh', '2016-10-27 18:59:39', '2016-10-27 18:59:39'),
(75, 4, 'dcwegerhg', 'games/1477569595.jpg', 'gregerg', '2016-10-27 18:59:55', '2016-10-27 18:59:55'),
(76, 4, 'hytjtyj', 'games/1477569617.jpg', 'jy5tjtyj', '2016-10-27 19:00:17', '2016-10-27 19:00:17'),
(77, 4, 'hrthewsh', 'games/1477569630.jpg', 'gerysgrh', '2016-10-27 19:00:30', '2016-10-27 19:00:30'),
(78, 4, 'trjtkjtuk', 'games/1477569648.jpg', 'hetjhrtjryj', '2016-10-27 19:00:48', '2016-10-27 19:00:48'),
(79, 4, 'nmndfgn', 'games/1477569686.jpg', 'bnsgngdn', '2016-10-27 19:01:26', '2016-10-27 19:01:26'),
(80, 4, 'vrweagaw', 'games/1477569701.jpg', 'gwragawg', '2016-10-27 19:01:41', '2016-10-27 19:01:41'),
(81, 4, 'gwrgweg', 'games/1477569717.jpg', 'gwegaweg', '2016-10-27 19:01:57', '2016-10-27 19:01:57'),
(82, 4, 'gwragwarg', 'games/1477569730.jpg', 'gwrgawerg', '2016-10-27 19:02:10', '2016-10-27 19:02:10'),
(83, 5, 'vasvfv', 'games/1477573324.jpg', 'fwefg', '2016-10-27 20:02:04', '2016-10-27 20:02:04'),
(84, 5, 'feqrfwt', 'games/1477573339.jpg', 'reqwrwrg', '2016-10-27 20:02:19', '2016-10-27 20:02:19'),
(85, 5, 'fmhnfgh', 'games/1477573385.jpg', 'nyjntyj', '2016-10-27 20:03:05', '2016-10-27 20:03:05'),
(86, 5, 'jtyjty', 'games/1477573402.jpg', 'jyjyj', '2016-10-27 20:03:22', '2016-10-27 20:03:22'),
(87, 5, 'ngdnfn', 'games/1477573522.jpg', 'besbtgh', '2016-10-27 20:05:22', '2016-10-27 20:05:22'),
(88, 5, 'degsth', 'games/1477573638.jpg', 'gersv', '2016-10-27 20:07:18', '2016-10-27 20:07:18'),
(89, 5, 'brtjrtj', 'games/1477573683.jpg', 'jrjry', '2016-10-27 20:08:03', '2016-10-27 20:08:03'),
(90, 5, 'j67tyjktyj', 'games/1477573697.jpg', 'jrsjth', '2016-10-27 20:08:17', '2016-10-27 20:08:17'),
(91, 5, 'jytejh', 'games/1477573712.jpg', 'jrjy', '2016-10-27 20:08:32', '2016-10-27 20:08:32'),
(92, 5, 'jsrjdtj', 'games/1477573728.jpg', 'rustjj', '2016-10-27 20:08:48', '2016-10-27 20:08:48'),
(93, 5, 'uykgm', 'games/1477573742.jpg', 'jtydj', '2016-10-27 20:09:02', '2016-10-27 20:09:02');

-- --------------------------------------------------------

--
-- Table structure for table `game_categories`
--

CREATE TABLE IF NOT EXISTS `game_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `game_categories`
--

INSERT INTO `game_categories` (`id`, `category_name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Virtual Reality', 'game-category/virtual-reality.jpg', 'Watching a movie in a VR theater is the next best thing to owning your own personal multiplex. Free from the distractions of the outside world, you’ll be completely immersed as you watch your favorite movies on a theater-sized scale. Step inside the game with Gear VR and be a part of the action like you''re really there. You can use your head movement to explore, aim, and interact – it''s an entirely new way to play. ', '2016-10-20 23:44:39', '2016-10-20 23:44:40'),
(2, 'Interactive Games', 'game-category/interactive.jpg', 'Watching a movie in a IG theater is the next best thing to owning your own personal multiplex. Free from the distractions of the outside world, you’ll be completely immersed as you watch your favorite movies on a theater-sized scale. Step inside the game with Gear VR and be a part of the action like you''re really there. You can use your head movement to explore, aim, and interact – it''s an entirely new way to play.', '2016-10-20 23:45:20', '2016-10-20 23:45:20'),
(3, 'Arcade Games', 'game-category/arcade.jpg', 'Watching a movie in a AG theater is the next best thing to owning your own personal multiplex. Free from the distractions of the outside world, you’ll be completely immersed as you watch your favorite movies on a theater-sized scale. Step inside the game with Gear VR and be a part of the action like you''re really there. You can use your head movement to explore, aim, and interact – it''s an entirely new way to play. ', '2016-10-20 23:45:48', '2016-10-20 23:45:48'),
(4, 'Room Based Games', 'game-category/room-based.jpg', 'Watching a movie in a RB theater is the next best thing to owning your own personal multiplex. Free from the distractions of the outside world, you’ll be completely immersed as you watch your favorite movies on a theater-sized scale. Step inside the game with Gear VR and be a part of the action like you''re really there. You can use your head movement to explore, aim, and interact – it''s an entirely new way to play. ', '2016-10-20 23:46:17', '2016-10-20 23:46:17'),
(5, 'Edutainment Games', 'game-category/edutainment.jpg', 'Watching a movie in a EG theater is the next best thing to owning your own personal multiplex. Free from the distractions of the outside world, you’ll be completely immersed as you watch your favorite movies on a theater-sized scale. Step inside the game with Gear VR and be a part of the action like you''re really there. You can use your head movement to explore, aim, and interact – it''s an entirely new way to play. ', '2016-10-20 23:46:42', '2016-10-20 23:46:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(19, '2014_10_12_000000_create_users_table', 1),
(20, '2014_10_12_100000_create_password_resets_table', 1),
(21, '2016_10_19_043736_create_game_categories', 1),
(22, '2016_10_19_043803_create_games', 1),
(23, '2016_10_19_043825_create_galleries', 1),
(24, '2016_10_19_043848_create_banners', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Shafeeque', 'shafeequebodhi@gmail.com', '$2y$10$y642CSwcwIdvQrmLqoY/CehFWEa/9Wewtn2GfcfYpcF1lUAgF5dxO', 'vXjVQKgqhksiDKynYNKYY4Q5j0aUNKSADoxlYtyPZvNmrsL7ZwXTRiuXmxQF', '2016-10-20 20:56:17', '2017-04-30 18:38:01');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
