<?php

namespace App\Http\Controllers\Admin;
use App\Game;
use App\GameCategory;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class GamesController extends Controller
{

    // Authentication
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Index
    public function index () {
        $games = Game::with('gameCategory')->get();
        //return $games;
        return view('admin.games.index', compact('games'));
    }

    // Add form
    public function showGameAddForm () {
        $categories = GameCategory::all();
        return view('admin.games.add-form', compact('categories'));
    }

    // Insert game
    public function store (Request $request, GameCategory $category) {
        $this->validate($request, [
            'title' => 'required',
            'game_category_id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'video_id' => 'required'
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/games'), $imageName);
        $game = new Game(["title" => $request -> title, "game_category_id" => $request -> game_category_id, "image" => "games/".$imageName, "video_id" => $request -> video_id]);
        // $status = $category -> addGame($game, $request -> game_category_id);
        $status = $game->save();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Game added successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('/admin/games');
    }

    // Show edit form
    public function showGameEditForm (Game $game) {
        $categories = GameCategory::all();
        return view('admin.games.edit-form')->with([
            'categories' => $categories,
            'game' => $game
        ]);
    }

    // Update game
    public function update (Request $request, Game $game) {
        $this->validate($request, [
            'title' => 'required',
            'game_category_id' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'video_id' => 'required'
        ]);
        $updateData = array("title" => $request -> title, "game_category_id" => $request -> game_category_id, "video_id" => $request -> video_id);
        if (Input::file()) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/games'), $imageName);
            unlink(public_path('uploads/'.$game->image));
            $updateData["image"] = 'games/'.$imageName;
        }
        $status = $game->update($updateData);
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Details updated successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('admin/games');
    }

    // Delete game
    public function delete (Game $game) {
        unlink(public_path('uploads/'.$game -> image));
        $status = $game->delete();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Game deleted successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('/admin/games');
    }
}