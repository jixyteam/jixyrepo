<?php

namespace App\Http\Controllers\Admin;
use App\Banner;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{

    // Authentication
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Index
    public function index () {
        $banners = Banner::all();
        return view('admin.banner.index', compact('banners'));
    }

    // Show add form
    public function showBannerForm () {
        return view('admin.banner.add-form');
    }

    // Inserting banner
    public function storeBanner (Request $request) {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/banner'), $imageName);
        $banner = new Banner;
        $banner -> image = 'banner/'.$imageName;
        $status = $banner -> save();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Banner image added successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('admin/banner');
    }

    // Delete banner
    public function delete (Banner $banner) {
        unlink(public_path('uploads/'.$banner->image));
        $status = $banner->delete();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Banner image deleted successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('admin/banner');
    }
}
