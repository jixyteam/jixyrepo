<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $feedbackData;
    public function __construct($feedbackData)
    {
        $this->feedbackData = $feedbackData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$formData = $this->feedbackData;
        //return $this->view('feedback', compact('formData'))->subject('Jixy Feedback');
        return $this->view('feedback')->subject('Jixy Feedback')->with([
            "name" => $this->feedbackData['name'],
            "email" => $this->feedbackData['email'],
            "contact" => $this->feedbackData['contact_number'],
            "subject" => $this->feedbackData['subject'],
            "bodyMessage" => $this->feedbackData['message']
        ]);
    }
}
