<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Route::get('/', 'FrontendController@index');
Route::get('/about', 'FrontendController@about');
Route::get('/games', 'FrontendController@games');
Route::get('/games/{category}', 'FrontendController@gameInner');
Route::get('/videos', 'FrontendController@videos');
Route::get('/videos/{category}', 'FrontendController@videoInner');
Route::get('/gallery', 'FrontendController@gallery');
Route::get('/contact', 'FrontendController@contact');
Route::post('/feedback', 'FrontendController@feedback');

Auth::routes();
Route::group(['middleware' => ['web']], function () {

});

/*
 * Routes for admin panel begin here
 */

Route::get('/home', 'HomeController@index');
Route::get('/admin/dashboard', 'HomeController@index'); // Dashboard
Route::get('/admin/game-categories', 'Admin\GameCategoryController@index'); // Game categories index

// Admin banner routes
Route::get('/admin/banner', 'Admin\BannerController@index'); // Banners index
Route::get('/admin/banner/add', 'Admin\BannerController@showBannerForm'); // Banner add form
Route::post('/admin/banner/store', 'Admin\BannerController@storeBanner'); // Add banner
Route::delete('/admin/banner/{banner}', 'Admin\BannerController@delete'); // Delete banner

// Admin gallery routes
Route::get('/admin/gallery', 'Admin\GalleryController@index'); // Gallery index
Route::get('/admin/gallery/add', 'Admin\GalleryController@showGalleryForm'); // Gallery add form
Route::get('/admin/gallery/{gallery}/edit', 'Admin\GalleryController@showGalleryEditForm'); // Gallery edit form
Route::post('/admin/gallery/store', 'Admin\GalleryController@storeGallery'); // Add gallery item
Route::patch('/admin/gallery/{gallery}', 'Admin\GalleryController@update'); // Update gallery item
Route::delete('/admin/gallery/{gallery}/delete', 'Admin\GalleryController@delete'); // Delete gallery item

// Admin game routes
Route::get('/admin/games', 'Admin\GamesController@index'); // Games index
Route::get('/admin/games/add', 'Admin\GamesController@showGameAddForm'); // Game add form
Route::get('/admin/games/{game}/edit', 'Admin\GamesController@showGameEditForm'); // Game edit form
Route::post('/admin/games/store', 'Admin\GamesController@store'); // Add game
Route::patch('/admin/games/{game}', 'Admin\GamesController@update'); // Update game
Route::delete('/admin/games/{game}/delete', 'Admin\GamesController@delete'); // Delete game
/*
 * Routes for admin panel end here
 */