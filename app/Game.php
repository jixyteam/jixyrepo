<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = ['title', 'game_category_id', 'image', 'video_id'];
    public function gameCategory () {
        return $this->belongsTo(GameCategory::class);
    }
}
