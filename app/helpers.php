<?php
use Illuminate\Support\Str;
function flash_status ($status = "info", $message) {
    session()->flash('status', $status);
    session()->flash('message', $message);
}
function limit_string_by_words ($str, $count = 15) {
    return Str::words($str, $count);
}