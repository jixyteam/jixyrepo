<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Jixy</title>
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css') }}/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    @yield('styles')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css') }}/main.css">

</head>
<body>
<div id="inner_page_header">
    <div class="inner_banner"><img src="{{ URL::asset('images') }}/about_banner.jpg"/></div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 left_logo"><img src="{{ URL::asset('images') }}/logo.png"></div>
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 header_bottom">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li {{ (Request::is('/')) ? 'class=active' : '' }}><a href="/">Home</a></li>
                <li {{ (Request::is('about')) ? 'class=active' : '' }}><a href="/about">About Us</a></li>
                <li {{ (Request::is('games')) ? 'class=active' : '' }}><a href="/games">Games</a></li>
                <li {{ (Request::is('videos')) ? 'class=active' : '' }}><a href="/videos">Videos</a></li>
                <li {{ (Request::is('gallery')) ? 'class=active' : '' }}><a href="/gallery">Gallery</a></li>
                <li {{ (Request::is('contact')) ? 'class=active' : '' }}><a href="/contact">Contact Us</a></li>
            </ul>
        </div>

    </div>
</div>
@yield('content')

<footer>
    <div class="container">
        <div class="row foot-row">

            <!--1st Col close here-->
            <div class="col-lg-3 col-md-3 col-sm-4">
                <div class="tab_2">
                    <div class="quick">
                        <p class="con_head">important links</p>
                    </div>
                    <div class="foot_nav">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/about">About Us</a></li>
                            <li><a href="/gallery">Our Gallery</a></li>
                            <li><a href="/games">Games</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--2nd Col starts here-->
            <div class="col-lg-2 col-md-2 col-sm-4">
                <div class="tab_2">

                    <div class="quick">
                        <p class="con_head" style="padding-top:25px;"></p>
                    </div>
                    <div class="foot_nav">
                        <ul>
                            <li><a href="/videos">Videos</a></li>
                            <li><a href="/contact">contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-5 col-bord">
                <div class="tab_2">
                    <div class="addrs">
                        <p class="con_head">Quick CONTACT</p>
                        <p><img src="{{ URL::asset('images') }}/locatin.png">Calicut
                            <br><img src="{{ URL::asset('images') }}/call.png">+91 9847 123456 / 996 123456
                    </div>
                </div>
            </div>
            <!--1st Col starts here-->
            <div class="col-lg-3 col-md-3 col-sm-3 col-bord">
                <iframe style="width:100%; height:150px; margin-top:20px" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d497699.99740305037!2d77.35073679427201!3d12.953847717752192!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae1670c9b44e6d%3A0xf8dfc3e8517e4fe0!2sBengaluru%2C+Karnataka!5e0!3m2!1sen!2sin!4v1476270637230" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>

        </div>
    </div>
    <!--Copyrigth bar starts here-->

    <div class="container-fluid footer_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="foot_bar">
                        <div class="copy"> © 2016 Jixy , All rights reserved, Powered by <a href="http://www.bodhiinfo.com" target="_blank"><img src="images/bodhi_logo.png"></a> </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Copyrigth bar close here-->
</footer>
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>--}}
<script src="{{ URL::asset('js') }}/jquery-2.2.4.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>--}}
<script src="{{ URL::asset('js') }}/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>

