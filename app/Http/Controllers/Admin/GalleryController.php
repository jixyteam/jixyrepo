<?php

namespace App\Http\Controllers\Admin;
use App\Gallery;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller
{

    // Authentication
    public function __construct()
    {
        $this->middleware('auth');
    }

    // Index
    public function index () {
        $galleries = Gallery::all();
        return view('admin.gallery.index', compact('galleries'));
    }

    // Show add form
    public function showGalleryForm () {
        return view('admin.gallery.add-form');
    }

    // Inserting gallery
    public function storeGallery (Request $request) {
        $this->validate($request, [
            'caption' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        $imageName = time().'.'.$request->image->getClientOriginalExtension();
        $request->image->move(public_path('uploads/gallery'), $imageName);
        $gallery = new Gallery;
        $gallery->caption = $request->caption;
        $gallery->image = 'gallery/'.$imageName;
        $status = $gallery->save();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Gallery image added successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('/admin/gallery');
    }

    // Show edit form
    public function showGalleryEditForm (Gallery $gallery) {
        return view('admin.gallery.edit-form', compact('gallery'));
    }

    // Update gallery
    public function update (Request $request, Gallery $gallery) {
        $this->validate($request, [
            'caption' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $updateData = array("caption" => $request->caption);
        if (Input::file()) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads/gallery'), $imageName);
            unlink(public_path('uploads/'.$gallery->image));
            $updateData["image"] = 'gallery/'.$imageName;
        }
        $status = $gallery->update($updateData);
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Details updated successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('admin/gallery');
    }

    // Delete gallery
    public function delete (Gallery $gallery) {
        unlink(public_path('uploads/'.$gallery->image));
        $status = $gallery->delete();
        if ($status) {
            $statusLabel = "success";
            $statusMsg = "Gallery item deleted successfully.";
        } else {
            $statusLabel = "danger";
            $statusMsg = "Some thing went wrong! Please try again.";
        }
        flash_status($statusLabel, $statusMsg);
        return redirect('admin/gallery');
    }
}
